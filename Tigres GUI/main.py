#!/bin/python
# Copyright (C) 2019 Cosme Jose Nieto Perez <cosmenp@gmail.com>
#
# This file is part of Tigres GUI.
#
#     Tigres GUI is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     any later version.
#
#     Tigres GUI is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with Tigres GUI.  If not, see <https://www.gnu.org/licenses/>.
#
# Autor : Cosme Jose Nieto Perez <cosmenp@gmail.com>

from Batalla import Batalla
from Tigre import Tigre

tigres = [Tigre(30, 20, 10, 10, 1, False, "Jugador"), Tigre(25, 10, 5, 5, 1, True, "Cachorro"),
          Tigre(10, 3, 2, 1, 2, True, "Normal Bajo"), Tigre(10, 3, 2, 1, 3, True, "Normal alto")]

Batalla("hola hola ", tigres).empezar()
