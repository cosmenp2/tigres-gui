# Copyright (C) 2019 Cosme Jose Nieto Perez <cosmenp@gmail.com>
#
# This file is part of Tigres GUI.
#
#     Tigres GUI is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     any later version.
#
#     Tigres GUI is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with Tigres GUI.  If not, see <https://www.gnu.org/licenses/>.
#
# Autor : Cosme Jose Nieto Perez <cosmenp@gmail.com>

class IA:

    def __init__(self, tigreUsusario, tigreIA, cooldown):
        self.tigreUsuario = tigreUsusario
        self.tigreIA = tigreIA
        self.cooldown = cooldown
        self.accion = 0  # 1 atacar, 2 curar, 0 no hace nada

    def valorarSituacion(self):
        if self.tigreIA.isDead() or self.tigreUsuario.isDead():
            self.accion = 0
        elif self.tigreUsuario.vida - self.tigreIA.ataque + self.tigreIA.ataque * 0.5 * (
                self.tigreUsuario.defensa / 100) <= 0:
            self.accion = 1
        elif self.cooldown == 0 and self.tigreIA.vida < 0.5 * self.tigreIA.vida_max:
            self.accion = 2
        else:
            self.accion = 1

    def realizarAccion(self):
        self.valorarSituacion()
        if self.accion == 1:
            self.tigreUsuario.recibirAtaque(self.tigreIA.ataque, self.tigreIA.habilidad)
        elif self.accion == 2:
            self.tigreIA.curar(7)

        return self.accion
