# Copyright (C) 2019 Cosme Jose Nieto Perez <cosmenp@gmail.com>
#
# This file is part of Tigres GUI.
#
#     Tigres GUI is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     any later version.
#
#     Tigres GUI is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with Tigres GUI.  If not, see <https://www.gnu.org/licenses/>.
#
# Autor : Cosme Jose Nieto Perez <cosmenp@gmail.com>

from tkinter import Tk, Image, FLAT, Button, Frame, Label, DISABLED, StringVar
from tkinter.ttk import Progressbar, Style

from PIL import Image, ImageTk

from IA import IA
from Pausa import Pausa


class Batalla:

    def __init__(self, nombre, tigres, numero_jugadores=1):
        self.nombre = nombre
        self.tigres = tigres
        self.numero_jugadores = numero_jugadores
        self.selected = 0
        self.cooldown = []
        for i in range(0, len(self.tigres)):
            self.cooldown.append(0)

    def empezar(self):
        root = Tk()
        root.title = f"Batle of tigers Level: {self.nombre}"
        root.geometry("900x500")
        root.maxsize(900, 500)
        root.minsize(900, 500)
        root.configure(background="white")

        # Boton pausa
        img = Image.open("images/pause.png")
        img = img.resize((35, 35), Image.ANTIALIAS)
        img = ImageTk.PhotoImage(img)
        pausa = Button(root, image=img, relief=FLAT, background="white", activebackground="lightgray",
                       command=lambda: Pausa(root, self.nombre, self.tigres, self.numero_jugadores).guiPausa())
        pausa.place(x=860, y=0)

        enemyframe = Frame(root, bg='white')

        if (len(self.tigres) - 1) == 3:
            enemyframe.place(x=50, y=30)
        elif (len(self.tigres) - 1) == 2:
            enemyframe.place(x=160, y=30)
        else:
            enemyframe.place(x=305, y=30)
        enemyframe.columnconfigure(0, weight=2 * (len(self.tigres) - 1))
        enemyframe.rowconfigure(0, weight=3)
        enemigos = []
        imagen = []
        seleccionado = []
        salud = []
        stats = []

        for i in range(1, len(self.tigres)):
            self.pintar_tigre(enemigos, enemyframe, imagen, seleccionado, salud, stats, i)

        for child in enemyframe.winfo_children(): child.grid_configure(padx=5, pady=5)

        userFrame = Frame(root, bg="white")
        userFrame.place(x=50, y=350)

        espaciousuario = []
        self.pintarTigreUsusario(userFrame, espaciousuario)

        for child in userFrame.winfo_children(): child.grid_configure(padx=5, pady=5)

        # Zona de resultado (Texto has ganado o perdido)
        resultadopos = Frame(root, bg="white")
        resultadopos.place(x=500, y=375)
        resultado = StringVar()
        resultado.set("")
        lb_resultado = Label(resultadopos, textvariable=resultado, bg="white")
        lb_resultado.grid(column=0, row=0)

        # Zona de botones
        accionBotones = Frame(root, bg="white")
        accionBotones.place(x=500, y=400)
        atacar = Button(accionBotones, text="Atacar", background="white", activebackground="lightgray", relief=FLAT,
                        command=lambda: self.atacar(enemigos, stats, salud, resultado, atacar, curar, pausa,
                                                    espaciousuario, textCurar, seleccionado))
        atacar.grid(column=0, row=1)
        textCurar = StringVar()
        textCurar.set("Curar")
        curar = Button(accionBotones, textvariable=textCurar, background="white", activebackground="lightgray",
                       relief=FLAT, command=lambda: self.curar(salud, resultado, atacar, curar, pausa,
                                                               espaciousuario, textCurar, seleccionado))
        curar.grid(column=1, row=1)

        for child in accionBotones.winfo_children(): child.grid_configure(padx=5, pady=5)

        self.seleccion_automatica(seleccionado)
        root.mainloop()

    def pintarTigreUsusario(self, userFrame, listadeobjetos):
        userTigre = self.tigres[0].obtenerImg(100)
        lb_tigre = Label(userFrame, image=userTigre, bg="white")
        lb_tigre.image = userTigre
        lb_tigre.grid(column=0, row=1)
        listadeobjetos.append(lb_tigre)
        userStats = Label(userFrame,
                          text=f"A : {self.tigres[0].ataque}\nD : {self.tigres[0].defensa}\nH : "
                          f"{self.tigres[0].habilidad}",
                          background="white", justify="left")
        userStats.grid(column=1, row=1)
        listadeobjetos.append(userStats)
        usersalud = Progressbar(userFrame, length=75, style='green.Horizontal.TProgressbar')
        usersalud['value'] = self.tigres[0].vida * 100 / self.tigres[0].vida_max
        usersalud.grid(column=0, row=2)
        listadeobjetos.append(usersalud)

    def pintar_tigre(self, enemigos, enemyframe, imagen, seleccionado, salud, stats, numero):
        columna = (numero - 1) * 2
        seleccionado.append(StringVar())
        seleccionado[numero - 1].set("")
        lb_selecionado = Label(enemyframe, textvariable=seleccionado[numero - 1], background="white")
        lb_selecionado.grid(column=columna, row=0)
        imagen.append(self.tigres[numero].obtenerImg())
        enemigos.append(Button(enemyframe, image=imagen[numero - 1], background="white",
                               activebackground="lightgray", relief=FLAT,
                               command=lambda: self.seleccionar(numero, seleccionado)))
        enemigos[numero - 1].grid(column=columna, row=1)
        enemigos[numero - 1].rowconfigure(0, weight=3)
        stats.append(Label(enemyframe,
                           text=f"A : {self.tigres[numero].ataque}\nD : {self.tigres[numero].defensa}\n"
                           f"H : {self.tigres[numero].habilidad}",
                           background="white", justify="left"))
        stats[numero - 1].grid(column=columna + 1, row=1)
        style = Style()
        style.theme_use('default')
        style.configure("green.Horizontal.TProgressbar", background='green', troughcolor="white")
        salud.append(Progressbar(enemyframe, length=100, style='green.Horizontal.TProgressbar'))
        salud[numero - 1]['value'] = self.tigres[numero].vida * 100 / self.tigres[numero].vida_max
        salud[numero - 1].grid(column=columna, row=2)

    def seleccionar(self, numero, lista, selecionar=True):
        for i in range(0, len(lista)):
            lista[i].set("")

        if selecionar:
            self.selected = numero
            lista[numero - 1].set("___\n"
                                  "\\*/\n"
                                  " \' ")

    def seleccion_automatica(self, lista):
        if not self.tigres[1].isDead():
            self.seleccionar(1, lista)
            return True
        elif len(self.tigres) - 1 > 1:
            if not self.tigres[2].isDead():
                self.seleccionar(2, lista)
                return True
            elif len(self.tigres) - 1 > 2:
                if not self.tigres[3].isDead():
                    self.seleccionar(3, lista)
                    return True
                else:
                    self.seleccionar(1, lista, False)
                    return False
            else:
                self.seleccionar(1, lista, False)
                return False
        else:
            self.seleccionar(1, lista, False)
            return False

    def atacar(self, enemigos, stats, salud, resultado, batacar, bcurar, bpausa, listaObjetosUsuario, textCurar, lista):
        bpausa.configure(state=DISABLED)
        self.tigres[self.selected].recibirAtaque(self.tigres[0].ataque, self.tigres[0].habilidad)
        salud[self.selected - 1]['value'] = self.tigres[self.selected].vida * 100 / self.tigres[self.selected].vida_max

        if (self.cooldown[0] != 0):
            self.cooldown[0] -= 1
            if self.cooldown[0] == 0:
                textCurar.set("Curar")
                bcurar.configure(state="normal")
            else:
                textCurar.set(f"Curar ({self.cooldown[0]})")

        if self.tigres[self.selected].isDead():
            enemigos[self.selected - 1].configure(state=DISABLED)
            stats[self.selected - 1].configure(state=DISABLED)
            estado = self.seleccion_automatica(lista)
            if not estado:
                resultado.set("Has Ganado")
                batacar.configure(state=DISABLED)
                bcurar.configure(state=DISABLED)
        self.llamadaIA(salud, listaObjetosUsuario, resultado, bcurar, batacar)
        bpausa.configure(state="normal")

    def curar(self, salud, resultado, batacar, bcurar, bpausa, listaObjetosUsuario, textCurar, lista):
        bpausa.configure(state=DISABLED)
        self.tigres[0].curar(7)
        listaObjetosUsuario[2]['value'] = self.tigres[0].vida * 100 / self.tigres[0].vida_max
        self.cooldown[0] = 3
        bcurar.configure(state=DISABLED)
        textCurar.set(f"Curar ({self.cooldown[0]})")
        self.llamadaIA(salud, listaObjetosUsuario, resultado, bcurar, batacar)

    def llamadaIA(self, salud, objetosUsuario, resultado, bcurar, batacar):
        for i in range(1, len(self.tigres)):
            accion = IA(self.tigres[0], self.tigres[i], self.cooldown[i]).realizarAccion()
            if accion == 1:
                objetosUsuario[2]['value'] = self.tigres[0].vida * 100 / self.tigres[0].vida_max
                if self.tigres[0].isDead():
                    objetosUsuario[0].configure(state=DISABLED)
                    objetosUsuario[1].configure(state=DISABLED)
                    resultado.set("Has perdido")
                    batacar.configure(state=DISABLED)
                    bcurar.configure(state=DISABLED)
                if self.cooldown[i] != 0:
                    self.cooldown[i] -= 1
            elif accion == 2:
                salud[i - 1]['value'] = self.tigres[i].vida * 100 / self.tigres[i].vida_max
                self.cooldown[i] = 3


def empezar(nombre, tigres, numero_jugadores):
    Batalla(nombre, tigres, numero_jugadores).empezar()
