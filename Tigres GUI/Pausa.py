# Copyright (C) 2019 Cosme Jose Nieto Perez <cosmenp@gmail.com>
#
# This file is part of Tigres GUI.
#
#     Tigres GUI is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     any later version.
#
#     Tigres GUI is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with Tigres GUI.  If not, see <https://www.gnu.org/licenses/>.

from tkinter import Tk, Button, FLAT, Frame

import Batalla


class Pausa:

    def __init__(self, root2, nombre, tigres, numero_jugadores):
        self.guiBatalla = root2
        self.nombre = nombre
        self.tigres = tigres
        self.numero_jugadores = numero_jugadores

    def guiPausa(self):
        root = Tk()
        root.title("Menú pausa")
        root.geometry("300x300")
        root.maxsize(300, 300)
        root.minsize(300, 300)
        root.focus_force()
        root.configure(background="white")

        botones = Frame(root, bg="white")
        botones.place(x=95, y=80)

        continuar = Button(botones, text="Continuar", relief=FLAT, background="white", activebackground="lightgray",
                           command=lambda: self.continuar(root))
        continuar.grid(column=0, row=0)

        reiniciar = Button(botones, text="Reiniciar", relief=FLAT, background="white", activebackground="lightgray",
                           command=lambda: self.reiniciar(root))
        reiniciar.grid(column=0, row=1)

        salir = Button(botones, text="Salir", relief=FLAT, background="white", activebackground="lightgray",
                       command=lambda: self.salir())
        salir.grid(column=0, row=2)

        for child in botones.winfo_children(): child.grid_configure(padx=10, pady=10)
        root.mainloop()

    def continuar(self, root):
        root.destroy()

    def reiniciar(self, root):
        for i in range(0, len(self.tigres)):
            self.tigres[i].restablecer()
        root.destroy()
        self.guiBatalla.destroy()
        Batalla.empezar(self.nombre, self.tigres, self.numero_jugadores)

    def salir(self):
        quit(0)
