# Copyright (C) 2019 Cosme Jose Nieto Perez <cosmenp@gmail.com>
#
# This file is part of Tigres GUI.
#
#     Tigres GUI is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     any later version.
#
#     Tigres GUI is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with Tigres GUI.  If not, see <https://www.gnu.org/licenses/>.
#
# Autor : Cosme Jose Nieto Perez <cosmenp@gmail.com>

import random

from PIL import Image, ImageTk


class Tigre:

    def __init__(self, vida_max, ataque, defensa, habilidad, nivel, enemigo, nombre):
        self.vida_max = vida_max
        self.ataque = ataque
        self.defensa = defensa
        self.habilidad = habilidad
        self.nivel = nivel
        self.nombre = nombre
        self.vida = self.vida_max
        self.enemigo = enemigo

    def restablecer(self):
        self.vida = self.vida_max

    def isDead(self):
        return self.vida == 0

    def obtenerImg(self, medida=200):
        nombre = f"images/{self.nivel}"
        if self.isDead():
            nombre += ".muerto"
        elif self.enemigo:
            nombre += ".enemigo"
        img = Image.open(nombre + ".png")
        img = img.resize((medida, medida), Image.ANTIALIAS)
        img = ImageTk.PhotoImage(img)
        return img

    def recibirAtaque(self, ataque, punteria):
        habilidad = self.habilidad - punteria
        defensa = ataque * 0.5 * (self.defensa / 100)
        danio = 0

        if habilidad >= random.randint(1, 100):
            return -1

        if self.vida - (ataque - defensa) < 0:
            danio = self.vida
            self.vida = 0
            return danio

        self.vida -= ataque - defensa
        return ataque - defensa

    def curar(self, curacion):
        if self.vida + curacion > self.vida_max:
            self.vida = self.vida_max
        else:
            self.vida += curacion
